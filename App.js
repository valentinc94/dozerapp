import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './src/Screens/home';
import Details from './src/Screens/details';

const Stack = createStackNavigator()

export default class App extends React.Component {
    render() {
        return (
          <NavigationContainer>
            <Stack.Navigator initialRouteName="Details">

              <Stack.Screen name="Home" component={Home} />
              <Stack.Screen name="Details" component={Details} />
              {/* <Stack.Screen name="Details">
                {props => <Details {...props} />}
              </Stack.Screen> */}

            </Stack.Navigator>
          </NavigationContainer>
          
        );
    }
}


// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Open up App.tsx to start working on your app!</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
