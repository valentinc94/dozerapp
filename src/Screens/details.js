import { StatusBar } from 'expo-status-bar';
import React, {Component} from 'react';
import { StyleSheet, Text, View, Dimensions, Button } from 'react-native';

var {height, width} = Dimensions.get('window');

export default class Details extends Component {

  constructor(props){
    super(props)
  }

  render(){
    return (
      <View style={styles.container}>
        <Text>Detail!</Text>
        <Button
            title="Go to Details"
            onPress={() => this.props.navigation.navigate('Home')}
            />
        <StatusBar style="auto" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});